package handlers

import (
	"fakSue/models"
	"net/http"
	"path/filepath"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Image struct {
	ID       uint   `json: "id"`
	Filename string `json:"filename"`
	UserID   uint   `json:"user_id"`
}

type CreateImageRes struct {
	Image
}

type ImageHandler struct {
	us  models.UserService
	ims models.ImageService
}

func NewImageHandler(us models.UserService, ims models.ImageService) *ImageHandler {
	return &ImageHandler{us, ims}
}

func (ih *ImageHandler) CreateImage(c *gin.Context) {
	image := new(Image)
	if err := c.BindJSON(image); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	res := new(models.Image)
	res.Filename = image.Filename
	res.UserID = image.UserID
	if err := ih.ims.CreateImage(res); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, res)
}

func (ih *ImageHandler) GetImageUser(c *gin.Context) {
	userIDStr := c.Param("id")
	id, err := strconv.Atoi(userIDStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	user, err := ih.us.GetByID(uint(id))
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	images, err := ih.ims.GetByUserID(user.ID)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := CreateImageRes{}
	r := CreateImageRes{}
	r.ID = images.ID
	r.UserID = user.ID
	r.Filename = filepath.Join(models.UserPath, userIDStr, images.Filename)

	c.JSON(http.StatusOK, res)
}
