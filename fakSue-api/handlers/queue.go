package handlers

import (
	"fakSue/models"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

type Queue struct {
	ID           uint    `json: "id"`
	UserId       uint    `json: "user_id"`
	RestId       uint    `json: "rest_id"`
	AmountPerson uint    `json: "amount_person"`
	Time         float32 `json: "time"`
	GotQueue     bool    `json: "got_queue"`
}

type ListQueue struct {
	ID           uint      `json: "id"`
	UserId       uint      `json: "user_id"`
	RestId       uint      `json: "rest_id"`
	AmountPerson uint      `json: "amount_person"`
	Time         float32   `json: "time"`
	GotQueue     bool      `json: "got_queue"`
	Date         time.Time `json: "date"`
	QueueImage   RestImage
	RestName     string
}

type RestInfo struct {
	RestName string `json: "rest_name"`
}

type Status struct {
	Status bool `json: "status"`
}

type CreateQueue struct {
	UserId       uint      `json: "user_id"`
	RestId       uint      `json: "rest_id"`
	AmountPerson uint      `json: "amount_person"`
	Time         float32   `json: "time"`
	GotQueue     bool      `json: "got_queue"`
}

type QueueHandler struct {
	rms models.RestImageService
	rs  models.RestaurantService
	us  models.UserService
	qs  models.QueueService
}

func NewQueueHanlder(rms models.RestImageService, rs models.RestaurantService, us models.UserService, qs models.QueueService) *QueueHandler {
	return &QueueHandler{rms, rs, us, qs}
}

func (qh *QueueHandler) CreateQueue(c *gin.Context) {
	data := new(CreateQueue)
	if err := c.BindJSON(data); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	queue := new(models.Queue)
	queue.UserId = data.UserId
	queue.RestId = data.RestId
	queue.AmountPerson = data.AmountPerson
	queue.Time = data.Time
	queue.GotQueue = data.GotQueue
	if err := qh.qs.CreateQueue(queue); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(201, gin.H{
		"id":           queue.ID,
		"UserId":       queue.UserId,
		"RestId":       queue.RestId,
		"AmountPerson": queue.AmountPerson,
		"Time":         queue.Time,
		"GotQueue":     queue.GotQueue,
		"Date":         queue.CreatedAt,
	})
}

func (qh *QueueHandler) ListQueueReserve(c *gin.Context) {
	userIDStr := c.Param("id")
	id, err := strconv.Atoi(userIDStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	user, err := qh.us.GetByID(uint(id))
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	queues, err := qh.qs.ListAllReserve(uint(user.ID))
	if err != nil {
		c.JSON(404, gin.H{
			"message": err.Error(),
		})
		return
	}

	res := []ListQueue{}
	for _, q := range queues {
		image, err := qh.rms.GetByRestID(q.RestId)
		if err != nil {
			c.JSON(500, gin.H{
				"message": err.Error(),
			})
		}
		rest, err := qh.rs.GetRestaurantByID(q.RestId)
		if err != nil {
			c.JSON(500, gin.H{
				"message": err.Error(),
			})
		}
		res = append(res, ListQueue{
			ID:           q.ID,
			UserId:       q.UserId,
			RestId:       q.RestId,
			AmountPerson: q.AmountPerson,
			Time:         q.Time,
			GotQueue:     q.GotQueue,
			Date:         q.CreatedAt,
			QueueImage: RestImage{
				ID:       image.ID,
				Filename: image.Filename,
				RestID:   image.ID,
			},
			RestName: rest.RestName,
		})
	}
	c.JSON(http.StatusOK, res)
}

type UpdateStatusReq struct {
	GotQueue bool
}

func (qh *QueueHandler) UpdateStatusQueue(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	req := new(UpdateStatusReq)
	if err := c.BindJSON(req); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	if _, err := qh.qs.GetByID(uint(id)); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	err = qh.qs.UpdateQueue(uint(id), req.GotQueue)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, gin.H{
		"message": "successful",
	})
}

func (qh *QueueHandler) ListQueueHistory(c *gin.Context) {
	userIDStr := c.Param("id")
	id, err := strconv.Atoi(userIDStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	user, err := qh.us.GetByID(uint(id))
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	queues, err := qh.qs.ListAllHistory(uint(user.ID))
	if err != nil {
		c.JSON(404, gin.H{
			"message": err.Error(),
		})
		return
	}

	res := []ListQueue{}
	for _, q := range queues {
		image, err := qh.rms.GetByRestID(q.RestId)
		if err != nil {
			c.JSON(500, gin.H{
				"message": err.Error(),
			})
		}
		rest, err := qh.rs.GetRestaurantByID(q.RestId)
		if err != nil {
			c.JSON(500, gin.H{
				"message": err.Error(),
			})
		}
		res = append(res, ListQueue{
			ID:           q.ID,
			UserId:       q.UserId,
			RestId:       q.RestId,
			AmountPerson: q.AmountPerson,
			Time:         q.Time,
			GotQueue:     q.GotQueue,
			Date:         q.CreatedAt,
			QueueImage: RestImage{
				ID:       image.ID,
				Filename: image.Filename,
				RestID:   image.ID,
			},
			RestName: rest.RestName,
		})
	}
	c.JSON(http.StatusOK, res)
}
