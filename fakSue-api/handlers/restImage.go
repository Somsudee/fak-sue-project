package handlers

import (
	"fakSue/models"
	"net/http"
	"path/filepath"
	"strconv"

	"github.com/gin-gonic/gin"
)

type RestImage struct {
	ID       uint   `json:"id"`
	Filename string `json:"filename"`
	RestID   uint   `json:"rest_id"`
}

type CreateRestImagesRes struct {
	RestImage
}

type RestImageHandler struct {
	rs  models.RestaurantService
	rms models.RestImageService
}

func NewRestImageHandler(rs models.RestaurantService, rms models.RestImageService) *RestImageHandler {
	return &RestImageHandler{rs, rms}
}

func (rih *RestImageHandler) CreateRestImage(c *gin.Context) {
	restIDStr := c.Param("id")
	id, err := strconv.Atoi(restIDStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	rest, err := rih.rs.GetByID(uint(id))
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	form, err := c.MultipartForm()
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	images, err := rih.rms.CreateRestImage(form.File["restPhotos"], rest.ID)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	res := []CreateRestImagesRes{}
	for _, img := range images {
		r := CreateRestImagesRes{}
		r.ID = img.ID
		r.RestID = rest.ID
		r.Filename = filepath.Join(models.RestUploadPath, restIDStr, img.Filename)
		res = append(res, r)
	}

	c.JSON(http.StatusOK, res)
}
