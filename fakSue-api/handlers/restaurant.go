package handlers

import (
	"fakSue/models"
	"log"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Restaurant struct {
	ID        uint    `json: "id"`
	RestName  string  `json: "rest_name"`
	Date      string  `json: "date"`
	Tel       string  `json: "tel"`
	OpenTime  float32 `json: "open_time"`
	CloseTime float32 `json: "close_time"`
	Latitude  float64 `json: "latitude"`
	Longitude float64 `json: "longitude"`
}

type GetRestaurant struct {
	ID        uint    `json: "id"`
	RestName  string  `json: "rest_name"`
	Date      string  `json: "date"`
	Tel       string  `json: "tel"`
	OpenTime  float32 `json: "open_time"`
	CloseTime float32 `json: "close_time"`
	Latitude  float64 `json: "latitude"`
	Longitude float64 `json: "longitude"`
	Image     FindRestImage
}

type FindRestImage struct {
	ID       uint   `json:"id"`
	Filename string `json:"filename"`
}

type RestaurantHandler struct {
	rs  models.RestaurantService
	rmg models.RestImageService
}

func NewRestaurantHandler(rs models.RestaurantService, rmg models.RestImageService) *RestaurantHandler {
	return &RestaurantHandler{rs, rmg}
}

func (rh *RestaurantHandler) CreateRestaurant(c *gin.Context) {
	data := new(Restaurant)
	if err := c.BindJSON(data); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	rest := new(models.Restaurant)
	rest.RestName = data.RestName
	rest.Tel = data.Tel
	rest.Date = data.Date
	rest.OpenTime = data.OpenTime
	rest.CloseTime = data.CloseTime
	rest.Latitude = data.Latitude
	rest.Longitude = data.Longitude
	if err := rh.rs.CreateRestaurant(rest); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.JSON(201, gin.H{
		"id":   rest.ID,
		"name": rest.RestName,
		"tel":  rest.Tel,
	})
}

func (rh *RestaurantHandler) ListRest(c *gin.Context) {
	rs, err := rh.rs.ListRestaurant()
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	rests := []GetRestaurant{}
	for _, r := range rs {
		image, err := rh.rmg.GetByRestID(uint(r.ID))
		if err != nil {
			c.JSON(500, gin.H{
				"message": err.Error(),
			})
			return
		}
		rests = append(rests, GetRestaurant{
			ID:        r.ID,
			RestName:  r.RestName,
			Date:      r.Date,
			Tel:       r.Tel,
			OpenTime:  r.OpenTime,
			CloseTime: r.CloseTime,
			Latitude:  r.Latitude,
			Longitude: r.Longitude,
			Image: FindRestImage{
				ID:       image.ID,
				Filename: image.Filename,
			},
		})
	}

	c.JSON(200, rests)
}

// Delete
func (rh *RestaurantHandler) DeleteRest(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
	}

	if err := rh.rs.DeleteRest(uint(id)); err != nil {
		log.Printf((err.Error()))
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.Status(204)
}

func (rh *RestaurantHandler) GetOneRest(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	oneRest, err := rh.rs.GetByID(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	oneImage, err := rh.rmg.GetByRestID(oneRest.ID)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
	}

	c.JSON(200, gin.H{
		"rest":      oneRest,
		"restImage": oneImage,
	})
}
