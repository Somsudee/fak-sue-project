package handlers

import (
	"fakSue/header"
	"fakSue/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

type UpdateUser struct {
	Username  string `json: "username"`
	FirstName string `json: "firstname"`
	LastName  string `json: "lastname"`
	Tel       string `json: "tel"`
	Gender    string `json: "gender"`
}

type UserHandler struct {
	ug  models.UserService
	ims models.ImageService
}

func NewUserHandler(ug models.UserService, ims models.ImageService) *UserHandler {
	return &UserHandler{ug, ims}
}

func (uh *UserHandler) CreateUser(c *gin.Context) {
	userReq := new(models.User)
	if err := c.BindJSON(userReq); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	user := new(models.User)
	user = userReq
	if err := uh.ug.CreateUser(user); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(201, gin.H{
		"token": user.Token,
	})
}

type LoginReq struct {
	Username string `json: "username"`
	Password string `json: "password"`
}

func (uh *UserHandler) Login(c *gin.Context) {
	req := new(LoginReq)
	if err := c.BindJSON(req); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	user := new(models.User)
	user.Username = req.Username
	user.Password = req.Password
	token, err := uh.ug.Login(user)
	if err != nil {
		c.JSON(401, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(201, gin.H{
		"token": token,
	})
}

func (uh *UserHandler) GetSession(c *gin.Context) {
	user, ok := c.Value("user").(*models.User)
	if !ok {
		c.JSON(401, gin.H{
			"message": "invalid token",
		})
		return
	}

	token := header.GetToken(c)
	if token == "" {
		c.JSON(401, gin.H{
			"message": "invalid token",
		})
		return
	}
	user, err := uh.ug.GetByToken(token)
	if err != nil {
		c.JSON(401, gin.H{
			"message": "user not found",
		})
		return
	}

	c.JSON(201, gin.H{
		"user":      user,
	})
}

func (uh *UserHandler) Logout(c *gin.Context) {
	token := header.GetToken(c)
	if token == "" {
		c.JSON(401, gin.H{
			"message": "user not found",
		})
		return
	}
	err := uh.ug.Logout(token)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.Status(204)
}

func (uh *UserHandler) Update(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"mesage": err.Error(),
		})
	}

	user := new(UpdateUser)
	if err := c.BindJSON(user); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	found, err := uh.ug.GetByID(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	found.Username = user.Username
	found.FirstName = user.FirstName
	found.LastName = user.LastName
	found.Tel = user.Tel
	found.Gender = user.Gender
	if err := uh.ug.Update(found); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	c.Status(204)
}
