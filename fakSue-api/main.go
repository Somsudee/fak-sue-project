package main

import (
	"fakSue/config"
	"fakSue/handlers"
	"fakSue/middleware"
	"fakSue/models"
	"log"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {
	conf := config.Load()

	db, err := gorm.Open("mysql", conf.Connection)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	if conf.Mode == "dev" {
		db.LogMode(true)
	}

	if err := db.AutoMigrate(
		&models.User{},
		&models.Image{},
		&models.Restaurant{},
		&models.Queue{},
		&models.RestImage{},
	).Error; err != nil {
		log.Fatal(err)
	}

	us := models.NewUserService(db, conf.HMACKey)
	rs := models.NewRestService(db)
	qs := models.NewQueueService(db)
	ims := models.NewImageService(db)
	rms := models.NewRestImageService(db)

	uh := handlers.NewUserHandler(us, ims)
	rh := handlers.NewRestaurantHandler(rs, rms)
	qh := handlers.NewQueueHanlder(rms, rs, us, qs)
	imh := handlers.NewImageHandler(us, ims)
	rmh := handlers.NewRestImageHandler(rs, rms)

	r := gin.Default()

	config := cors.DefaultConfig()
	config.AllowHeaders = []string{"authorization", "content-type"}
	config.AllowAllOrigins = true
	r.Use(cors.New(config))

	r.Static("/userPic", "./userPic")
	r.Static("/RestPic", "./RestPic")

	r.POST("/register", uh.CreateUser)
	r.POST("/login", uh.Login)
	//restaurant
	r.POST("/restaurants", rh.CreateRestaurant)
	r.GET("/restaurants", rh.ListRest)
	r.GET("/restaurants/:id", rh.GetOneRest)
	r.DELETE("/restaurants/:id", rh.DeleteRest)
	r.POST("/restaurant/:id/image", rmh.CreateRestImage)

	auth := r.Group("/")
	auth.Use(middleware.RequiredUser(us))
	{
		auth.POST("/user/image", imh.CreateImage)
		auth.POST("/logout", uh.Logout)
		auth.PUT("/user/:id", uh.Update)
		auth.GET("/user/:id/image", imh.GetImageUser)
		auth.GET("/session", func(c *gin.Context) {
			user, ok := c.Value("user").(*models.User)
			if !ok {
				c.JSON(401, gin.H{
					"message": "invalid token",
				})
				return
			}
			c.JSON(200, user)
		})
		auth.GET("/user", uh.GetSession)
		auth.POST("/queues", qh.CreateQueue)
		auth.GET("/queues/:id/reserve", qh.ListQueueReserve)
		auth.GET("/queues/:id/history", qh.ListQueueHistory)
		auth.PATCH("/queues/:id", qh.UpdateStatusQueue)
	}
	r.Run()
}
