package middleware

import (
	"fakSue/models"
	"fmt"
	"strings"

	"github.com/gin-gonic/gin"
)

func RequiredUser(ug models.UserService) gin.HandlerFunc {
	return func(c *gin.Context) {
		header := c.GetHeader("Authorization")
		header = strings.TrimSpace(header)
		if len(header) <= 7 {
			fmt.Printf("ummm")
			c.Status(401)
			c.Abort()
			return
		}

		token := strings.TrimSpace(header[len("Bearer "):])
		if token == "" {
			fmt.Printf("herree")
			c.JSON(401, gin.H{
				"message": "invalid token",
			})
			c.Abort()
			return
		}

		user, err := ug.GetByToken(token)
		if err != nil {
			fmt.Printf("stoppp")
			c.JSON(401, gin.H{
				"message": "unauthorized",
			})
			c.Abort()
			return
		}
		c.Set("user", user)
	}
}
