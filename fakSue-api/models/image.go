package models

import (
	"io"
	"log"
	"mime/multipart"
	"os"
	"path/filepath"
	"strconv"

	"github.com/jinzhu/gorm"
)

const Upload = "upload"
const UserPath = "userPic"

type Image struct {
	gorm.Model
	Filename string `gorm:"not null" json:"filename"`
	UserID   uint   `gorm:"not null" json:"user_id"`
}

type ImageService interface {
	CreateImage(image *Image) error
	Delete(id uint) error
	GetByUserID(id uint) (*Image, error)
}

type ImageGorm struct {
	db *gorm.DB
}

func NewImageService(db *gorm.DB) ImageService {
	return &ImageGorm{db}
}

func (ims *ImageGorm) CreateImage(image *Image) error {
	return ims.db.Create(image).Error
}

func (img *Image) UserFilePath() string {
	idStr := strconv.FormatUint(uint64(img.UserID), 10)
	return filepath.Join(UserPath, idStr, img.Filename)
}

// func (ims *ImageGorm) CreateImage(files []*multipart.FileHeader, userID uint) ([]Image, error) {
// 	idStr := strconv.FormatUint(uint64(userID), 10)
// 	dir := filepath.Join(UserPath, idStr)
// 	if err := os.MkdirAll(dir, os.ModePerm); err != nil {
// 		return nil, err
// 	}

// 	tx := ims.db.Begin()
// 	defer func() {
// 		if r := recover(); r != nil {
// 			tx.Rollback()
// 		}
// 	}()

// 	if err := tx.Error; err != nil {
// 		return nil, err
// 	}

// 	images := []Image{}
// 	for _, file := range files {
// 		image := Image{
// 			UserID:   userID,
// 			Filename: file.Filename,
// 		}
// 		if err := tx.Create(&image).Error; err != nil {
// 			tx.Rollback()
// 			return nil, err
// 		}
// 		images = append(images, image)

// 		if err := saveFile(file, image.UserFilePath()); err != nil {
// 			tx.Rollback()
// 			return nil, err
// 		}
// 	}

// 	if err := tx.Commit().Error; err != nil {
// 		return nil, err
// 	}
// 	return images, nil
// }

func saveFile(file *multipart.FileHeader, dst string) error {
	src, err := file.Open()
	if err != nil {
		return err
	}
	defer src.Close()

	out, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, src)
	return err
}

func (ims *ImageGorm) GetByID(id uint) (*Image, error) {
	image := new(Image)
	err := ims.db.First(image, "id = ?", id).Error
	if err != nil {
		return nil, err
	}
	return image, nil
}

func (ims *ImageGorm) Delete(id uint) error {
	image, err := ims.GetByID(id)
	if err != nil {
		return err
	}
	err = os.Remove(image.UserFilePath())
	if err != nil {
		log.Printf("Fail deleting image: %v\n", err)
	}
	return ims.db.Where("id = ?", id).Delete(&Image{}).Error
}

func (ims *ImageGorm) GetByUserID(id uint) (*Image, error) {
	images := new(Image)
	err := ims.db.
		Order("").
		Where("user_id = ?", id).
		Find(&images).Error
	if err != nil {
		return nil, err
	}
	return images, nil
}
