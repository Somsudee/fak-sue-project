package models

import (
	"github.com/jinzhu/gorm"
)

type Queue struct {
	gorm.Model
	UserId       uint    `gorm:"not null" json: "user_id"`
	RestId       uint    `gorm:"not null" json: "rest_id"`
	AmountPerson uint    `gorm:"not null" json: "amount_person"`
	Time         float32 `gorm:"not null" json: "time"`
	GotQueue     bool    `gorm:"not null" json: "got_queue"`
}

type QueueService interface {
	CreateQueue(queue *Queue) error
	GetByID(id uint) (*Queue, error)
	UpdateQueue(id uint, GotQueue bool) error
	ListQueue() ([]Queue, error)
	UpdateStatusQueue(id uint, GotQueue bool) error
	GetByUserID(id uint) ([]Queue, error)
	ListQueueStatus(queue []Queue, status bool) ([]Queue, error)
	ListAllReserve(id uint) ([]Queue, error)
	ListAllHistory(id uint) ([]Queue, error)
}

var _ QueueService = &QueueGorm{}

type QueueGorm struct {
	db *gorm.DB
}

func NewQueueService(db *gorm.DB) QueueService {
	return &QueueGorm{db}
}

func (qg *QueueGorm) CreateQueue(queue *Queue) error {
	return qg.db.Create(queue).Error
}

func (qg *QueueGorm) GetByID(id uint) (*Queue, error) {
	queue := new(Queue)
	if err := qg.db.Where("id = ?", id).First(queue).Error; err != nil {
		return nil, err
	}
	return queue, nil
}

func (qg *QueueGorm) UpdateQueue(id uint, GotQueue bool) error {
	return qg.db.Model(&Queue{}).Where("id = ?", id).Update("got_queue", GotQueue).Error
}

func (qg *QueueGorm) ListQueue() ([]Queue, error) {
	queues := []Queue{}
	if err := qg.db.Find(&queues).Error; err != nil {
		return nil, err
	}
	return queues, nil
}

func (qg *QueueGorm) UpdateStatusQueue(id uint, GotQueue bool) error {
	return qg.db.Model(&Queue{}).Where("id = ?", id).Update("got_queue", GotQueue).Error
}

func (qg *QueueGorm) GetByUserID(id uint) ([]Queue, error) {
	queues := []Queue{}
	err := qg.db.
		Order("").
		Where("user_id = ?", id).
		Find(&queues).Error
	if err != nil {
		return nil, err
	}
	return queues, nil
}

func (qg *QueueGorm) ListQueueStatus(queue []Queue, status bool) ([]Queue, error) {
	queues := []Queue{}
	err := qg.db.
		Order("created_at DESC").
		Where("got_queue", status).
		Find(&queues).Error
	if err != nil {
		return nil, err
	}
	return queues, nil
}

func (qg *QueueGorm) ListAllReserve(id uint) ([]Queue, error) {
	queues := []Queue{}
	err := qg.db.
		Where("got_queue = ?", false).
		Find(&queues).Error
	if err != nil {
		return nil, err
	}
	for i := 0; i < len(queues); i++ {
		final := []Queue{}
		err := qg.db.
			Where("user_id = ?", id).
			Find(&final).Error
		if err != nil {
			return nil, err
		}
		queues = final
	}
	return queues, nil
}

func (qg *QueueGorm) ListAllHistory(id uint) ([]Queue, error) {
	queues := []Queue{}
	err := qg.db.
		Where("got_queue = ?", true).
		Find(&queues).Error
	if err != nil {
		return nil, err
	}
	for i := 0; i < len(queues); i++ {
		final := []Queue{}
		err := qg.db.
			Where("user_id = ?", id).
			Find(&final).Error
		if err != nil {
			return nil, err
		}
		queues = final
	}
	return queues, nil
}
