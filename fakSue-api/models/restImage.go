package models

import (
	"log"
	"mime/multipart"
	"os"
	"path/filepath"
	"strconv"

	"github.com/jinzhu/gorm"
)

const RestUploadPath = "RestPic"

type RestImage struct {
	gorm.Model
	Filename string `gorm:"not null" json:"filename"`
	RestID   uint   `gorm:"not null" json:"rest_id"`
}

type RestImageService interface {
	Delete(id uint) error
	GetByRestID(id uint) (*RestImage, error)
	CreateRestImage(files []*multipart.FileHeader, restID uint) ([]RestImage, error)
}

type RestImageGorm struct {
	db *gorm.DB
}

func NewRestImageService(db *gorm.DB) RestImageService {
	return &RestImageGorm{db}
}

func (img *RestImage) RestFilePath() string {
	idStr := strconv.FormatUint(uint64(img.RestID), 10)
	return filepath.Join(RestUploadPath, idStr, img.Filename)
}

func (rms *RestImageGorm) CreateRestImage(files []*multipart.FileHeader, restID uint) ([]RestImage, error) {
	idStr := strconv.FormatUint(uint64(restID), 10)
	dir := filepath.Join(RestUploadPath, idStr)
	if err := os.MkdirAll(dir, os.ModePerm); err != nil {
		return nil, err
	}

	tx := rms.db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	if err := tx.Error; err != nil {
		return nil, err
	}

	restImages := []RestImage{}
	for _, file := range files {
		restImage := RestImage{
			RestID:   restID,
			Filename: file.Filename,
		}
		if err := tx.Create(&restImage).Error; err != nil {
			tx.Rollback()
			return nil, err
		}
		restImages = append(restImages, restImage)

		if err := saveFile(file, restImage.RestFilePath()); err != nil {
			tx.Rollback()
			return nil, err
		}
	}

	if err := tx.Commit().Error; err != nil {
		return nil, err
	}
	return restImages, nil
}

func (rms *RestImageGorm) GetByID(id uint) (*RestImage, error) {
	restImage := new(RestImage)
	err := rms.db.First(restImage, "id = ?", id).Error
	if err != nil {
		return nil, err
	}
	return restImage, nil
}

func (rms *RestImageGorm) Delete(id uint) error {
	image, err := rms.GetByID(id)
	if err != nil {
		return err
	}
	err = os.Remove(image.RestFilePath())
	if err != nil {
		log.Printf("Fail deleting image: %v\n", err)
	}
	return rms.db.Where("id = ?", id).Delete(&Image{}).Error
}

func (rms *RestImageGorm) GetByRestID(id uint) (*RestImage, error) {
	images := new(RestImage)
	err := rms.db.
		Order("").
		Where("rest_id = ?", id).
		Find(&images).Error
	if err != nil {
		return nil, err
	}
	return images, nil
}
