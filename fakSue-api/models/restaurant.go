package models

import (
	"path/filepath"
	"strconv"

	"github.com/jinzhu/gorm"
)

type Restaurant struct {
	gorm.Model
	RestName  string  `gorm:"not null" json: "rest_name"`
	Tel       string  `gorm:"not null" json: "tel"`
	Date      string  `gorm:"not null" json: "date"`
	OpenTime  float32  `gorm:"not null" json: "open_time"`
	CloseTime float32  `gorm:"not null" json: "close_time"`
	Latitude  float64 `gorm:"not null" json: "latitude"`
	Longitude float64 `gorm:"not null" json: "longitude"`
}

type RestaurantService interface {
	CreateRestaurant(rest *Restaurant) error
	ListRestaurant() ([]Restaurant, error)
	GetRestaurantByID(id uint) (*Restaurant, error)
	GetByID(id uint) (*Restaurant, error)
	DeleteRest(id uint) error
}

var _ RestaurantService = &RestGorm{}

type RestGorm struct {
	db *gorm.DB
}

func NewRestService(db *gorm.DB) RestaurantService {
	return &RestGorm{db}
}

func (rg *RestGorm) CreateRestaurant(rest *Restaurant) error {
	return rg.db.Create(rest).Error
}

func (restImg *RestImage) FilePath() string {
	idStr := strconv.FormatUint(uint64(restImg.RestID), 10)
	return filepath.Join(RestUploadPath, idStr, restImg.Filename)
}

func (rg *RestGorm) ListRestaurant() ([]Restaurant, error) {
	rests := []Restaurant{}
	if err := rg.db.Find(&rests).Error; err != nil {
		return nil, err
	}
	return rests, nil
}

func (rg *RestGorm) GetRestaurantByID(id uint) (*Restaurant, error) {
	rt := new(Restaurant)
	if err := rg.db.First(rt, id).Error; err != nil {
		return nil, err
	}
	return rt, nil
}

func (rg *RestGorm) GetByID(id uint) (*Restaurant, error) {
	rest := new(Restaurant)
	err := rg.db.Where("id = ?", id).First(rest).Error
	if err != nil {
		return nil, err
	}
	return rest, nil
}

func (rg *RestGorm) DeleteRest(id uint) error {
	rt := new(Restaurant)
	if err := rg.db.Where("id = ?", id).First(rt).Error; err != nil {
		return err
	}
	return rg.db.Delete(rt).Error
}
