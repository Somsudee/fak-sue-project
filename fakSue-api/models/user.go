package models

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fakSue/rand"
	"fmt"
	"hash"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	gorm.Model
	Token     string `json: "token"`
	Username  string `gorm:"unique_index; not null" json: "username"`
	Password  string `gorm:"not null" json: "password"`
	FirstName string `gorm:"not null" json: "firstname"`
	LastName  string `gorm:"not null" json: "lastname"`
	Tel       string `gorm:"not null" json: "tel"`
	Gender    string `gorm:"not null" json: "gender"`
}

type UserService interface {
	CreateUser(user *User) error
	Login(user *User) (string, error)
	GetByToken(token string) (*User, error)
	Logout(token string) error
	GetByID(id uint) (*User, error)
	Update(user *User) error
}

type UserGorm struct {
	db   *gorm.DB
	hmac hash.Hash
}

func NewUserService(db *gorm.DB, key string) UserService {
	hmac := hmac.New(sha256.New, []byte(key))
	return &UserGorm{db, hmac}
}

const cost = 12

func (ug *UserGorm) CreateUser(temp *User) error {
	user := new(User)
	user = temp
	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), cost)
	if err != nil {
		return err
	}
	user.Password = string(hash)
	token, err := rand.GetToken()
	if err != nil {
		return err
	}

	fmt.Println("token ===> ", token)
	ug.hmac.Write([]byte(token))
	tokenHash := ug.hmac.Sum(nil)
	ug.hmac.Reset()
	tokenHashStr := base64.URLEncoding.EncodeToString(tokenHash)
	fmt.Println("tokenHashStr ===> ", tokenHashStr)

	user.Token = tokenHashStr
	temp.Token = token

	return ug.db.Create(user).Error
}

func (ug *UserGorm) Login(user *User) (string, error) {
	found := new(User)
	err := ug.db.Where("username = ?", user.Username).First(&found).Error
	if err != nil {
		return "", err
	}
	err = bcrypt.CompareHashAndPassword([]byte(found.Password), []byte(user.Password))
	if err != nil {
		return "", err
	}
	token, err := rand.GetToken()
	if err != nil {
		return "", err
	}

	fmt.Println("token ===> ", token)

	ug.hmac.Write([]byte(token))
	tokenHash := ug.hmac.Sum(nil)
	ug.hmac.Reset()
	tokenHashStr := base64.URLEncoding.EncodeToString(tokenHash)
	fmt.Println("tokenHashStr ===> ", tokenHashStr)

	err = ug.db.Model(&User{}).
		Where("id = ?", found.ID).
		Update("token", tokenHashStr).Error
	if err != nil {
		return "", err
	}
	return token, nil
}

func (ug *UserGorm) GetByToken(token string) (*User, error) {
	ug.hmac.Write([]byte(token))
	tokenHash := ug.hmac.Sum(nil)
	ug.hmac.Reset()
	tokenHashStr := base64.URLEncoding.EncodeToString(tokenHash)

	user := new(User)
	err := ug.db.Where("token = ?", tokenHashStr).First(user).Error
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (ug *UserGorm) Logout(token string) error {
	user, err := ug.GetByToken(token)
	if err != nil {
		return err
	}
	return ug.db.Model(&User{}).
		Where("id = ?", user.ID).
		Update("token", "").Error
}

func (ug *UserGorm) GetByID(id uint) (*User, error) {
	user := new(User)
	err := ug.db.First(user, "id = ?", id).Error
	if err != nil {
		return nil, err
	}
	return user, err
}

func (ug *UserGorm) Update(user *User) error {
	found := new(User)
	if err := ug.db.Where("id = ?", user.ID).First(found).Error; err != nil {
		return err
	}
	return ug.db.Model(user).Update(user).Error
}
