/**
 * @format
 */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import Route from './src/router/Route';

AppRegistry.registerComponent(appName, () => Route);
