import axios from "axios";

export const host = "https://linebackend2.mooo.com";

//User
export function CreateUser(User) {
  return axios.post(`${host}/register`, User)
}

export function CreateUserImage(form, id,token) {
  console.log(form, token)
  return axios.post(`${host}/user/image`, {
    Filename: form,
    UserID: id,
  }, {
    headers: {
      Authorization: `Bearer ` + token,
    }
  })
}

export function OnLogin(Username, Password) {
  return axios.post(`${host}/login`, {
    Username,
    Password,
  })
}

export function OnLogout(token) {
  return axios.post(`${host}/logout`, {}, {
    headers: {
      Authorization: `Bearer ` + token,
    }
  })
}

export function getUser(token) {
  return axios.get(`${host}/user`, {
    headers: {
      Authorization: `Bearer ` + token,
    }
  })
}

export function UpdateUser(User, token) {
  return axios.get(`${host}/user`, {
    User
  }, {
    headers: {
      Authorization: `Bearer ` + token,
    }
  })
}

//queue
export function CreateQueue(queue, token) {
  console.log(queue, token)
  return axios.post(`${host}/queues`, {
    UserId: queue.UserId,
    RestId: queue.RestId,
    AmountPerson: queue.AmountPerson,
    Time: queue.Time,
    GotQueue: queue.GotQueue,
  }, {
    headers: {
      Authorization: `Bearer ` + token,
    }
  })
}

export function UpdateStatusQueue(queueId, GotQueue, token) {
  return axios.put(`${host}/queues/${queueId}`, {
    GotQueue
  }, {
    headers: {
      Authorization: `Bearer ` + token,
    }
  })
}

export function GetListReserve(token, id) {
  return axios.get(`${host}/queues/${id}/reserve`, {
    headers: {
      Authorization: `Bearer ` + token,
    }
  })
}

export function GetListHistory(token, id) {
  return axios.get(`${host}/queues/${id}/history`, {
    headers: {
      Authorization: `Bearer ` + token,
    }
  })
}

//rest
export function GetRest() {
  return axios.get(`${host}/restaurants`)
}

export function GetRestImage(id) {
  return axios.get(`${host}/restaurant/${id}/image`)
}

export function GetOneRest(id) {
  return axios.get(`${host}/restaurants/${id}`)
}