export const GET_LOGIN = "GET_LOGIN";
export const GET_REST_INFO = "GET_REST_INFO";
export const GET_USER_INFO_FOR_ALL = "GET_USER_INFO_FOR_ALL";

export const getUserToken = (token) => {
  return {
    type: GET_LOGIN,
    token,
  }
}

export const getRestInfo = (rest) => {
  return {
    type: GET_REST_INFO,
    rest,
  }
}

export const getUserInfoForAll = (user) => {
  return {
    type: GET_USER_INFO_FOR_ALL,
    user,
  }
}