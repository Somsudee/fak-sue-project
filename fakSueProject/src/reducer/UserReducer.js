import { GET_LOGIN, GET_REST_INFO, GET_USER_INFO_FOR_ALL } from 'src/reducer/UserAction.js';

const initialUser = {
  token: '',
  oneRest: {},
  userInfoReducer: {},
}

export const UserReducer = (state = initialUser, action) => {
  switch (action.type) {
    case GET_LOGIN:
      return {
        ...state,
        token: action.token,
      }
    case GET_REST_INFO:
      return {
        ...state,
        oneRest: action.rest,
      }
    case GET_USER_INFO_FOR_ALL:
      console.log(action.user.ID)
      return {
        ...state,
        userInfoReducer: action.user,
      }
    default:
      return state
  }
}