import { combineReducers } from "redux";
import { UserReducer } from "../reducer/UserReducer";

const rootReducer = combineReducers({
  userReducer: UserReducer,
})
export default rootReducer;
