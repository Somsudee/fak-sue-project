import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Register from 'src/screen/OtherScreen/Register/Register';
import BottomTab from './BottomTab';
import FoodInfo from '../screen/OtherScreen/FoodInfo/FoodInfo';
import UserPhoto from '../screen/OtherScreen/Register/UserPhoto/UserPhoto';
import Profile from '../screen/OtherScreen/Profile/Profile';
import Login from '../screen/OtherScreen/Login/Login';
import ReserveQueue from '../screen/OtherScreen/ReserveQueue/ReserveQueue';
import EditProfile from '../screen/OtherScreen/EditProfile/EditProfile';
import ShopperTab from './ShopperTab';

export default App = () => {

  const Stack = createStackNavigator();

  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
    {/* <Stack.Screen name="ShopperTab" component={ShopperTab} /> */}
      <Stack.Screen name="BottomTab" component={BottomTab} />
      {/* main */}
      <Stack.Screen name="FoodInfo" component={FoodInfo} />
      <Stack.Screen name="ReserveQueue" component={ReserveQueue} />
      {/* menu */}
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="EditProfile" component={EditProfile} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="UserPhoto" component={UserPhoto} />
    </Stack.Navigator>
  );
}