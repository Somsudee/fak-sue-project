import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { IconButton } from 'react-native-paper';
import ShopperTab from 'src/router/ShopperTab';
import Menu from 'src/screen/MainScreen/Menu/Menu';
import Main from 'src/screen/MainScreen/Main/Main';

export default BottomTab = () => {

  const Tab = createBottomTabNavigator();
  return (
    <Tab.Navigator
      initialRouteName="Main"
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;
          if (route.name === 'Main') {
            iconName = focused ? 'home-variant' : 'home-variant-outline';
          } else if (route.name === 'Queue') {
            iconName = focused ? 'ticket' : 'ticket-outline';
          } else if (route.name === 'Menu') {
            iconName = focused ? 'account-circle' : 'account-circle-outline';
          }
          return <IconButton icon={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: '#FFA26B',
        inactiveTintColor: '#C4C4C4',
      }}
    >
      <Tab.Screen name='Main' component={Main} />
      <Tab.Screen name='Queue' component={ShopperTab} />
      <Tab.Screen name='Menu' component={Menu} />
    </Tab.Navigator>
  )
}