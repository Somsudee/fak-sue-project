import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Main from 'src/screen/MainScreen/Main/Main';
import FoodInfo from 'src/screen/OtherScreen/FoodInfo/FoodInfo';
import ReserveQueue from 'src/screen/OtherScreen/ReserveQueue/ReserveQueue';

export default MainRoute = () => {
  const Stack = createStackNavigator();

  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Main" component={Main} />
      <Stack.Screen name="FoodInfo" component={FoodInfo} />
      <Stack.Screen name="ReserveQueue" component={ReserveQueue} />
    </Stack.Navigator>
  );
}
