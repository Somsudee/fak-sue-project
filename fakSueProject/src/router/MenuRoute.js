import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Menu from 'src/screen/MainScreen/Menu/Menu';

const MenuRoute = () => {
  const Stack = createStackNavigator();

  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Menu" component={Menu} />
      <Stack.Screen name="Profile" component={Profile} />
    </Stack.Navigator>
  )
}
export default MenuRoute;
