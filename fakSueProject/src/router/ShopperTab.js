import React, { useState } from 'react';
import { Dimensions, View, Text } from 'react-native';
import {
  TabView, SceneMap, TabBar,
} from 'react-native-tab-view';
import Queue from 'src/screen/MainScreen/QueueMenu/Queue';
import History from 'src/screen/MainScreen/QueueMenu/History';
import { ShopperStyle } from 'src/router/Style/ShopperStyle';
import { Modal, Button } from 'react-native-paper';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { SafeAreaView } from 'react-native-safe-area-context';

const initialLayout = { width: Dimensions.get('window').width };

const ShopperTab = (props) => {
  const [index, setIndex] = useState(0);
  const [routes] = useState([
    { key: 'queue', title: 'queue' },
    { key: 'history', title: 'history' },
  ]);

  const { token } = props;
  const navigation = useNavigation();

  const renderScene = SceneMap({
    queue: Queue,
    history: History,
  });

  return (
    <SafeAreaView style={ShopperStyle.container} contentContainerStyle={{ flexGrow: 1 }}>
      <View style={ShopperStyle.container}>
        <View style={ShopperStyle.header} >
          <Text style={ShopperStyle.topic}>Reserved</Text>
        </View>
        <TabView
          navigationState={{ index, routes }}
          renderScene={renderScene}
          onIndexChange={setIndex}
          initialLayout={initialLayout}
          renderTabBar={(props) => {
            return (
              <TabBar
                {...props}
                activeColor="#FFA26B"
                inactiveColor="#c2c3c8"
                indicatorStyle={{ backgroundColor: '#FFA26B' }}
                style={{ backgroundColor: '#fff' }}
              />
            )
          }}
        />
      </View>
      <Modal
        visible={
          token == '' ?
            true
            :
            false
        }
        style={ShopperStyle.modalContainer}
      >
        <View style={ShopperStyle.modalBox}>
          <Button color='#F49015' mode='text' uppercase={false} onPress={() => navigation.navigate('Login')}>Login</Button>
          <Text style={{ color: '#C4C4C4', marginTop: 16 }}>for use this menu</Text>
        </View>
      </Modal>
    </SafeAreaView>
  );
}
const mapStateToProps = state => {
  return {
    token: state.userReducer.token,
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ShopperTab);