import { StyleSheet } from "react-native";

export const ShopperStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 100,
    backgroundColor: '#fff',
    borderBottomRightRadius: 40,
    borderBottomLeftRadius: 40,
    marginBottom: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  topic: {
    flex: 1,
    fontSize: 32,
    marginLeft: 24,
    color: '#183747',
  },
  modalContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalBox: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: 144,
    borderRadius: 8,
    backgroundColor: "#fff",
    marginHorizontal: 32,
  },

})