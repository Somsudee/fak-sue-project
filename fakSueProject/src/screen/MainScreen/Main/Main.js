import React, { useState, useEffect } from 'react';
import { View, SafeAreaView, Text, ScrollView, FlatList, Image, TouchableHighlight } from 'react-native';
import { MainStyle } from 'src/screen/MainScreen/Main/MainStyle';
import { Searchbar, Chip } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
import { GetRest, host } from '../../../api';

const Main = () => {
  const [restList, setRestList] = useState([]);
  const [textSearch, setTextSearch] = useState('');
  const [filterRest, setFilter] = useState([]);
  const [timeFilter, setTimeFilter] = useState('');

  const navigation = useNavigation();
  useEffect(() => {
    fetchRest()
  }, [])

  const fetchRest = () => {
    GetRest()
      .then((res) => setRestList(res.data))
      .catch((err) => console.error(err))
  }
  console.log(restList)
  const filterTime = (time) => {
    setTimeFilter(time)
    if (time == '') {
      restList
    } else if (time == 6) {
      restList.filter((rest) => {
        for (var i = time; i > 12; i++) {
          rest.OpenTime.match(i)
        }
      })
    } else if (time == 12) {
      restList.filter((rest) => {
        for (var i = time; i > 18; i++) {
          rest.OpenTime.match(i)
        }
      })
    } else if (time == 18) {
      restList.filter((rest) => {
        for (var i = time; i >= 24; i++) {
          rest.OpenTime.match(i)
        }
      })
    }
  }

  const Item = ({ item }) => {
    return (
      <View style={MainStyle.listContainer}>
        <TouchableHighlight onPress={() => navigation.navigate('FoodInfo', { id: item.ID })}>
          <View>
            <Image
              style={MainStyle.foodImage}
              source={{ uri: `${host}/RestPic/${item.ID}/${item.Image.filename}` }}
            />
            <View style={MainStyle.spaceText}>
              <Text style={[MainStyle.detailText, { fontSize: 16 }]}>{item.RestName.toUpperCase()}</Text>
            </View>
          </View>
        </TouchableHighlight>
      </View>
    );
  }

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }} contentContainerStyle={{ flexGrow: 1 }}>
      <ScrollView>
        <View style={MainStyle.contianer}>
          <View style={MainStyle.header} >
            <Text style={MainStyle.topic}>LineUp</Text>
          </View>
          <Searchbar
            placeholder="hunger wait for no man..."
            onChangeText={text => setTextSearch(text)}
            value={textSearch}
            style={MainStyle.searchBox}
          />
          <ScrollView horizontal={true} style={MainStyle.filterScroll}>
            <Chip onPress={() => filterTime('')} style={MainStyle.spaceChip}>All</Chip>
            <Chip onPress={() => filterTime(6)} style={MainStyle.spaceChip}>6:00 - 12:00</Chip>
            <Chip onPress={() => filterTime(12)} style={MainStyle.spaceChip}>12:00 - 18:00</Chip>
            <Chip onPress={() => filterTime(18)} style={MainStyle.spaceChip}>18:00 - 22:00</Chip>
          </ScrollView>
          <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
            <FlatList
              data={
                textSearch != '' && timeFilter == '' ?
                  restList.filter(rest => rest.RestName.toUpperCase().match(textSearch.toUpperCase()))
                  :
                  timeFilter != '' ?
                    filterRest
                    :
                    restList
              }
              renderItem={({ item }) =>
                <Item
                  id={item.ID}
                  item={item}
                />
              }
              keyExtractor={item => item.ID}
            />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView >
  )
}

export default Main;