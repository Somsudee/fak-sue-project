import { StyleSheet } from "react-native";

export var MainStyle = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
    backgroundColor: '#fff',
    borderBottomRightRadius: 40,
    borderBottomLeftRadius: 40,
    marginBottom: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,

    elevation: 6,
  },
  topic: {
    flex: 1,
    fontSize: 32,
    marginLeft: 24,
    color: '#183747',
  },
  searchBox: {
    marginHorizontal: 16,
    marginVertical: 8,
    height: 40,
    backgroundColor: '#fff',
  },
  filterScroll: {
    flexDirection: 'row',
    marginLeft: 8,
  },
  spaceChip: {
    marginHorizontal: 4,
    marginVertical: 8
  },
  text: {
    marginLeft: 16,
    marginVertical: 8,
    color: '#6C6C6C',
  },
  image: {
    height: 50,
    width: 100,
    borderRadius: 10,
  },
  listContainer: {
    flexDirection: 'row',
    alignItems: 'stretch',
    marginBottom: 16,
    marginHorizontal: 4,
  },
  foodImage: {
    height: 150,
    width: 344,
    borderRadius: 10,
    marginBottom: 4,
  },
  spaceText: {
    marginHorizontal: 4,
    marginBottom: 8,
    width: 328,
  },
  detailText: {
    color: '#6C6C6C',
    fontWeight: "bold",
    marginLeft: 8,
  }
})