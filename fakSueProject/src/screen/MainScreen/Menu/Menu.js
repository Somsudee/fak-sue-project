import React from 'react';
import { MenuStyle } from 'src/screen/MainScreen/Menu/MenuStyle';
import { ScrollView, View, SafeAreaView, Text } from 'react-native';
import { List, Button, Divider } from 'react-native-paper';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUserToken, getUserInfoForAll } from 'src/reducer/UserAction';
import { useNavigation } from '@react-navigation/native';
import { OnLogout } from '../../../api';

const Menu = (props) => {
  const navigation = useNavigation();
  const { token, getUserToken, getUserInfoForAll } = props;

  const clearTokenLogout = () => {
    OnLogout(token)
      .then(() => {
        getUserToken('')
        getUserInfoForAll('')
      })
      .catch(err => console.error(err))
  }

  return (
    <SafeAreaView style={MenuStyle.container} contentContainerStyle={{ flexGrow: 1 }}>
      <ScrollView style={MenuStyle.innerContainer}>
        <View style={MenuStyle.header} >
          <Text style={MenuStyle.topic}>Menu</Text>
        </View>
        {
          token == '' ?
            <View>
              <List.Item
                title="Profile"
                description="login to unlock"
                left={() => <List.Icon style={MenuStyle.disableIcon} />}
                right={() => <List.Icon color='#C4C4C4' icon="chevron-right" />}
                style={MenuStyle.disableList}
              />
            </View>
            :
            <View>
              <List.Item
                title="Profile"
                left={() => <List.Icon color='#ffffff' icon="account" style={MenuStyle.listReserve} />}
                right={() => <List.Icon color='#000000' icon="chevron-right" />}
                onPress={() => navigation.navigate('Profile')}
              />
            </View>
        }
        <Divider style={{ borderWidth: 0.5, marginHorizontal: 20, marginVertical: 8, borderColor: '#DEDEDE' }} />
        <View style={MenuStyle.containerButton}>
          {
            token == '' ?
              <Button icon="login" mode="text" color='#ffffff' onPress={() => navigation.navigate('Login')} style={MenuStyle.loginButton}>LOGIN</Button>
              :
              <Button icon="logout" mode="text" color='#ffffff' onPress={() => clearTokenLogout()} style={MenuStyle.logoutButton}>LOGOUT</Button>
          }
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}
const mapStateToProps = state => {
  return {
    token: state.userReducer.token,
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ getUserToken, getUserInfoForAll }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
