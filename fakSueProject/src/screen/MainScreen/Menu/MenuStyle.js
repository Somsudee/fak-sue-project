import { StyleSheet } from "react-native";

export var MenuStyle = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  innerContainer: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  backButton: {
    margin: 0,
  },
  header: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    height: 100,
    backgroundColor: '#fff',
    borderBottomRightRadius: 40,
    borderBottomLeftRadius: 40,
    marginBottom: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  topic: {
    flex: 1,
    fontSize: 32,
    marginLeft: 20,
    color: '#183747',
  },
  listReserve: {
    backgroundColor: '#FBCC88',
    borderRadius: 20,
    paddingLeft: 5
  },
  listBussiness: {
    backgroundColor: '#BDE5FA',
    borderRadius: 20,
  },
  disableIcon: {
    backgroundColor:'#6C6C6C',
    borderRadius: 20,
    opacity: 0.4
  },
  disableList:{
    opacity: 0.4
  },
  divider: {
    borderBottomWidth: 1,
    borderColor: '#DEDED3',
    paddingBottom: 20,
  },
  containerButton: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'center',
    marginTop: 20,
  },
  loginButton: {    
    margin: 5,
    paddingHorizontal: 10,
    paddingVertical: 5,
    backgroundColor: '#FFA26B',
    borderRadius: 10,
  },
  logoutButton: {    
    margin: 5,
    paddingHorizontal: 10,
    paddingVertical: 5,
    backgroundColor: '#66B5F8',
    borderRadius: 10,
  },
})
