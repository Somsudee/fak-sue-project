import React, { useState, useCallback } from 'react';
import { View, ScrollView, FlatList, Image, Text } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { useFocusEffect } from '@react-navigation/native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { RequestedStyle } from 'src/screen/MainScreen/QueueMenu/Style/RequestedStyle';
import { host, GetListHistory } from '../../../api';
import { Avatar } from 'react-native-paper';

const History = (props) => {
  const [queueList, setQueueList] = useState([]);

  const { token, userInfoReducer } = props;
  // console.log(userInfoReducer.user.ID)

  const fetchQueue = () => {
    if (token == '') {
      return queueList
    } else if (token != '') {
      GetListHistory(token, userInfoReducer.user.ID)
        .then((res) => setQueueList(res.data))
        .catch((err) => console.log(err))
    }
  }

  useFocusEffect(
    useCallback(() => {
      fetchQueue();
    }, [queueList])
  );

  const Item = ({ item }) => {
    console.log(item)
    return (
      <View style={RequestedStyle.listContainer}>
        <View style={{ flexDirection: 'column' }}>
          <Image
            style={RequestedStyle.image}
            source={{ uri: `${host}/RestPic/${item.QueueImage.id}/${item.QueueImage.filename}` }}
          />
        </View>
        <View style={RequestedStyle.info}>
          <Text style={RequestedStyle.listTitle}>{item.RestName}</Text>
          <View style={{ flexDirection: 'row' }}>
            <Text style={RequestedStyle.listTitle}>{item.AmountPerson}</Text>
            <Avatar.Icon size={24} icon="account-multiple" />
          </View>
          <View style={{ flexDirection: 'row' }}>
            <Text style={RequestedStyle.listTitle}>{item.Time}</Text>
            <Avatar.Icon size={24} icon="clock-outline" />
          </View>
        </View>
      </View>
    );
  }

  return (
    <SafeAreaView style={RequestedStyle.container} contentContainerStyle={{ flexGrow: 1 }}>
      <ScrollView style={{ flex: 1 }}>
        <View style={RequestedStyle.FlatListContainer}>
          {
            queueList != '' ?
              <FlatList
                data={queueList}
                renderItem={({ item }) => <Item item={item} />}
                keyExtractor={item => item.id}
              />
              :
              <Text style={RequestedStyle.textNoData}>No reserve queue</Text>
          }
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const mapStateToProps = state => {
  return {
    token: state.userReducer.token,
    userInfoReducer: state.userReducer.userInfoReducer,
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(History);