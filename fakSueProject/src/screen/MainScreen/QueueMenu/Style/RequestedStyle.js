import { StyleSheet } from 'react-native';

export var RequestedStyle = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  FlatListContainer: {
    flex: 1,
    height: 400,
    width: 400,
    justifyContent: 'center',
    alignItems: 'center'
  },
  listContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'stretch',
    height: 120,
    width: 350,
    marginBottom: 4,
  },
  listTitle: {
    flex: 1,
    flexDirection: 'column',
    fontSize: 20,
    color: '#6C6C6C',
    marginTop: 8,
  },
  textNoData: {
    fontSize: 24,
    fontWeight: '600',
    color: '#C1C1C1',
  },
  image: {
    height: 120,
    width: 160,
    borderRadius:8
  },
  info: {
    flexDirection: 'column',
    width: 120,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 16,
  },
  text: {
    flex: 1,
    flexDirection: 'column',
    fontSize: 16,
    color: '#C4C4C4',
    margin: 4,
  }
})