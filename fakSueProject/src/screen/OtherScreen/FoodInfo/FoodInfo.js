import React, { useState, useRef, useEffect } from 'react';
import { Image, View, ScrollView, SafeAreaView, Text } from 'react-native';
import { FoodInfoStyle } from 'src/screen/OtherScreen/FoodInfo/FoodInfoStyle';
import { Divider, List, Button, IconButton, Modal } from 'react-native-paper';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import { check, PERMISSIONS, RESULTS } from 'react-native-permissions';
import { useNavigation } from '@react-navigation/native';
import { GetOneRest, host } from '../../../api';
import { Marker } from 'react-native-maps';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getRestInfo } from '../../../reducer/UserAction'

const FoodInfo = (props) => {
  const [rest, setRest] = useState([]);
  const [restImage, setRestImage] = useState([]);
  const [name, setName] = useState("");
  const [visible, setVisible] = useState(false);
  const [position, setPosition] = useState({
    latitude: 18.750902,
    longitude: 98.946169,
  })

  const { token, getRestInfo } = props;
  const navigation = useNavigation();
  const MapViewRef = useRef();

  const callLocation = (callback) => {
    Geolocation.getCurrentPosition(
      () => {
        callback()
      },
      (error) => {
        console.log(error.code, error.message);
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    );
  }

  useEffect(() => {
    GetOneRest(props.route.params.id)
      .then(res => {
        setRest(res.data.rest)
        setRestImage(res.data.restImage)
        setName(res.data.rest.RestName)
        getRestInfo(res.data)
        setPosition({
          latitude: res.data.rest.Latitude,
          longitude: res.data.rest.Longitude,
        })
      })
      .catch(err => console.error(err))

    check(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
      .then(result => {
        if (result === RESULTS.GRANTED) {
          callLocation(() => {
            MapViewRef.current.animateCamera({
              center: {
                latitude: position.latitude,
                longitude: position.longitude,
              }
            })
          })
          return
        }
        if (result === RESULTS.DENIED) {
          request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then(result => {
            if (result === RESULTS.GRANTED) {
              callLocation(() => {
                MapViewRef.current.animateCamera({
                  center: {
                    latitude: position.latitude,
                    longitude: position.longitude,
                  }
                })
              });
            }
            else if (result === RESULTS.DENIED) {
              alert('Denied is confirmed');
            }
            else if (result === RESULTS.BLOCKED) {
              alert('Blocked is confirmed');
            }
          })
          return
        }
        if (result === RESULTS.BLOCKED) {
          Alert.alert(
            'Warning',
            'Open setting to allow to access.',
            [
              { text: 'Open setting', onPress: () => openSettings() },
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              { text: 'OK', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false },
          );
        }
      })
  }, [])

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }} contentContainerStyle={{ flexGrow: 1 }}>
      <ScrollView style={{ flex: 1 }}>
        <View style={FoodInfoStyle.contianer}>
          <View style={{ flex: 1, height: 800 }}>
            <Image
              style={FoodInfoStyle.image}
              source={{ uri: `${host}/RestPic/${restImage.ID}/${restImage.filename}` }}
            />
            <IconButton
              icon="arrow-left-drop-circle"
              color='#FFA26B'
              size={30}
              onPress={() => navigation.navigate('Main')}
              style={FoodInfoStyle.backButton}
            />
          </View>
          <View style={FoodInfoStyle.containerInfo}>
            <View style={FoodInfoStyle.containerText}>
              <View style={FoodInfoStyle.spaceSixteen}>
                <Text style={FoodInfoStyle.foodFont}>{name.toUpperCase()}</Text>
              </View>
              <View style={FoodInfoStyle.containerDate}>
                <View style={FoodInfoStyle.innerContainer}>
                  <List.Icon color='#6C6C6C' icon="calendar-remove" style={FoodInfoStyle.icon} />
                  <Text style={[FoodInfoStyle.fadeText, { marginLeft: 4 }]}>{rest.Date}</Text>
                </View>
                <View style={FoodInfoStyle.innerContainer}>
                  <List.Icon color='#6C6C6C' icon="clock-outline" style={FoodInfoStyle.icon} />
                  <Text style={[FoodInfoStyle.fadeText, { marginLeft: 4 }]}>{rest.OpenTime} - {rest.CloseTime}</Text>
                </View>
              </View>
              <Divider style={FoodInfoStyle.divider} />
              <View style={FoodInfoStyle.spaceEight}>
                <List.Icon color='#FFA26B' icon="phone" style={FoodInfoStyle.icon} />
                <Text style={[FoodInfoStyle.fadeText, { marginLeft: 4 }]}>{rest.Tel}</Text>
              </View>
              <Divider style={FoodInfoStyle.divider} />
              <View style={FoodInfoStyle.makeOrder}>
                <Text style={[FoodInfoStyle.fadeText, { color: '#FFA26B' }]}>Reserve now?</Text>
                <Button
                  color='#fff'
                  mode='text'
                  style={FoodInfoStyle.button}
                  onPress={() => {
                    token == '' ?
                      setVisible(true)
                      :
                      navigation.navigate('ReserveQueue', { id: rest.ID })
                  }}>YES</Button>
              </View>
              <View style={FoodInfoStyle.mapAllContainer}>
                <View style={FoodInfoStyle.mapHeader}>
                  <Text style={[FoodInfoStyle.mapText, { fontSize: 24 }]}>{rest.RestName}'s Location</Text>
                </View>
                <View style={FoodInfoStyle.mapContainer}>
                  <MapView
                    ref={MapViewRef}
                    provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                    style={FoodInfoStyle.map}
                    region={{
                      latitude: position.latitude,
                      longitude: position.longitude,
                      latitudeDelta: 0.015,
                      longitudeDelta: 0.0121,
                    }}>
                    <Marker
                      coordinate={{
                        latitude: position.latitude,
                        longitude: position.longitude,
                      }}
                    />
                  </MapView>
                </View>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
      <Modal
        visible={visible}
        onDismiss={() => setVisible(false)}
        style={FoodInfoStyle.modalContainer}
      >
        <View style={FoodInfoStyle.modalBox}>
          <Button color='#F49015' mode='text' uppercase={false} onPress={() => navigation.navigate('Login')}>Login</Button>
          <Text style={{ color: '#C4C4C4', marginTop: 16 }}>before reserve queue</Text>
        </View>
      </Modal>
    </SafeAreaView >
  )
}
const mapStateToProps = state => {
  return {
    token: state.userReducer.token,
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ getRestInfo }, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(FoodInfo);