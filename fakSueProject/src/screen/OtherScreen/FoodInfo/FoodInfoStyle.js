import { StyleSheet } from "react-native";

export var FoodInfoStyle = StyleSheet.create({
  contianer: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  backButton: {
    position: 'absolute',
    top: 5,
    bottom: 0,
    right: 0,
    left: 0,
  },
  image: {
    height: 248,
    width: 370,
  },
  containerInfo: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 210,
    bottom: 0,
    right: 0,
    left: 0,
    backgroundColor: '#fff',
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    padding: 8,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  rearrage: {
    position: 'absolute',
    top: -80,
    bottom: 0,
    right: 0,
    left: 0,
    alignItems: 'center',
  },
  containerText: {
    flex: 3,
    alignItems: 'center',

  },
  fadeText: {
    color: '#6C6C6C',
    marginHorizontal: 16,
  },
  spaceSixteen: {
    margin: 16,
    alignItems: 'center',
  },
  spaceEight: {
    margin: 8,
    flexDirection: 'row',
  },
  foodFont: {
    fontSize: 32,
    fontWeight: 'bold'
  },
  containerDate: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: 250,
  },
  innerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    margin: 0,
    marginLeft: 8,
    height: 20,
    width: 25
  },
  divider: {
    margin: 8,
    width: 300,
    borderWidth: 0.5,
    borderColor: '#EBEBEB',
  },
  makeOrder: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 16,
  },
  button: {
    paddingHorizontal: 4,
    borderRadius: 8,
    backgroundColor: '#FFA26B',
  },
  mapHeader: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 70,
    borderTopEndRadius: 20,
    borderTopStartRadius: 20,
    backgroundColor: '#FBCC88',
    width: 300,
  },
  mapAllContainer: {
    height: 300,
    width: 300,
  },
  mapContainer: {
    height: 250,
    width: 300,
  },
  mapText: {
    color: '#fff',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    width: 300,
  },
  modalContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalBox: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: 136,
    borderRadius: 8,
    backgroundColor: "#fff",
    marginHorizontal: 40,
  },
})