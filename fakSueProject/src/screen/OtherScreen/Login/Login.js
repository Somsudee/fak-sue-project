import React, { useState } from 'react';
import { LoginStyle } from 'src/screen/OtherScreen/Login/LoginStyle';
import { View, ScrollView, Text, Image, SafeAreaView } from 'react-native';
import { Card, Button, IconButton, TextInput, Divider, HelperText } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUserToken, getUserInfoForAll } from '../../../reducer/UserAction'
import { OnLogin, getUser } from '../../../api';

const Login = (props) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [securePassword, setSecurePassword] = useState(true);
  const [visible, setVisible] = useState(false);

  const checkUsername = username && username.length > 0;
  const checkPassword = password && password.length > 0;

  const navigation = useNavigation();
  const { getUserToken, getUserInfoForAll } = props;

  const loginUser = () => {
    OnLogin(username, password)
      .then(res => {
        getUserToken(res.data.token)
        getUser(res.data.token)
          .then(res => {
            console.log(res.data)
            getUserInfoForAll(res.data)
            navigation.navigate('Menu')
          })
          .catch(err => console.error(err))
      })
      .catch((err) => {
        if (err != null) {
          setVisible(true)
        }
      })
  }

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }} contentContainerStyle={{ flexGrow: 1 }}>
      <ScrollView style={{ flex: 1 }}>
        <IconButton
          icon="arrow-left-drop-circle"
          color='#FFA26B'
          size={30}
          onPress={() => navigation.goBack()}
        />
        <Card style={LoginStyle.cardMainContainer}>
          <View style={LoginStyle.header}>
            <Text style={LoginStyle.title}>Let's Login</Text>
          </View>
          <Card.Content style={LoginStyle.contentContainer}>
            <Image
              style={LoginStyle.logoImage}
              source={require('src/asset/male_2.png')}
            />
            <TextInput
              mode='flat'
              label='Username'
              value={username}
              onChangeText={text => setUsername(text)}
              style={{ backgroundColor: '#fff', width: 250, }}
            />
            <View style={LoginStyle.containerPassword}>
              <TextInput
                style={{ backgroundColor: '#fff', width: 210, }}
                label='Password'
                value={password}
                mode="flat"
                secureTextEntry={securePassword}
                onChangeText={text => setPassword(text)}
              />
              <IconButton
                icon=
                {
                  securePassword == true ?
                    "eye-off"
                    :
                    "eye-outline"
                }
                style={{ marginTop: 35 }}
                color='#6C6C6C'
                size={20}
                onPress={() => {
                  securePassword == true ?
                    setSecurePassword(false)
                    :
                    setSecurePassword(true)
                }}
              />
            </View>
            <HelperText
              type="error"
              visible={visible}
            >
              Username or Password is incorrect
            </HelperText>
          </Card.Content>
          <Card.Actions style={[LoginStyle.contentContainer, { flex: 0 }]}>
            <Button color='#ffffff' mode='text' style={!checkUsername || !checkPassword ? LoginStyle.disableButton : LoginStyle.button} disabled={!checkUsername || !checkPassword ? true : false} onPress={() => loginUser()}>LOGIN</Button>
          </Card.Actions>
          <View style={LoginStyle.containerDivider}>
            <Divider style={LoginStyle.divider} />
            <Text style={LoginStyle.text}>or</Text>
            <Divider style={LoginStyle.divider} />
          </View>
          <View style={LoginStyle.footer}>
            <Text style={LoginStyle.text}>Don’t have an account?</Text>
            <Button color='#F49015' mode='text' onPress={() => navigation.navigate('Register')}>Register</Button>
          </View>
        </Card>
      </ScrollView>
    </SafeAreaView>
  );
}
const mapStateToProps = state => {
  return {
    token: state.userReducer.token,
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ getUserToken, getUserInfoForAll }, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(Login);