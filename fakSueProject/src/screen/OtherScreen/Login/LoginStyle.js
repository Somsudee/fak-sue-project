import { StyleSheet } from "react-native";

export var LoginStyle = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  cardMainContainer: {
    flex: 3,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
    marginHorizontal: 24,
    marginBottom: 24,
    borderRadius: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  header: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 64,
    borderTopEndRadius: 20,
    borderTopStartRadius: 20,
    backgroundColor: '#FBCC88',
  },
  title: {
    color: '#fff',
    fontSize: 24,
  },
  contentContainer: {
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 16,
  },
  logoImage: {
    width: 120,
    height: 125,
    borderRadius: 8,
  },
  containerPassword: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'stretch',
    padding: 8,
  },
  text: {
    color: '#8E8E8E',
  },
  button: {
    borderRadius: 8,
    marginLeft: 8,
    marginBottom: 8,
    paddingVertical: 4,
    paddingHorizontal: 24,
    backgroundColor: '#FFA26B',
  },
  disableButton: {
    borderRadius: 8,
    marginLeft: 8,
    marginBottom: 8,
    paddingVertical: 4,
    paddingHorizontal: 24,
    backgroundColor: '#DEDEDE',
  },
  containerDivider: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  divider: {
    borderWidth: 0.5,
    borderColor: '#C4C4C4',
    width: 100,
    marginHorizontal: 20,
  },
  footer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 16,
    marginTop: 8
  },

})