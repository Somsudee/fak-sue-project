import React, { useEffect, useState } from 'react';
import { View, ScrollView, Text, Image, Alert } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUser } from '../../../api';
import { SafeAreaView } from 'react-native-safe-area-context';
import { IconButton } from 'react-native-paper';
import { ProfileStyle } from './ProfileStyle';
import { useNavigation } from '@react-navigation/native';

const Profile = (props) => {
  const [userInfo, setUserInfo] = useState([]);
  const [userImage, setUserImage] = useState([]);

  const { token } = props;
  const navigation = useNavigation();

  useEffect(() => {
    fetchUser()
  }, [])

  const fetchUser = () => {
    getUser(token)
      .then(res => {
        setUserInfo(res.data.user)
        setUserImage(res.data.userImage)
      })
      .catch(err => {
        Alert.alert("Sorry...","We suggest you to login again.")
        navigation.navigate("Menu")
        })
  }

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }} contentContainerStyle={{ flexGrow: 1 }}>
      <ScrollView style={{ flex: 1 }}>
        <View style={ProfileStyle.header} >
          <IconButton
            icon="chevron-left"
            color='#183747'
            size={32}
            onPress={() => navigation.navigate('Menu')}
            style={ProfileStyle.backButton}
          />
          <Text style={ProfileStyle.topic}>PROFILE</Text>
        </View>
        <View>
          <View style={ProfileStyle.text}>
            <Text style={ProfileStyle.innerText}>Firstname:</Text>
            <Text style={ProfileStyle.innerText}>{userInfo.FirstName}</Text>
          </View>
          <View style={ProfileStyle.text}>
            <Text style={ProfileStyle.innerText}>Lastname:</Text>
            <Text style={ProfileStyle.innerText}>{userInfo.LastName}</Text>
          </View>
          <View style={ProfileStyle.text}>
            <Text style={ProfileStyle.innerText}>Username:</Text>
            <Text style={ProfileStyle.innerText}>{userInfo.Username}</Text>
          </View>
          <View style={ProfileStyle.text}>
            <Text style={ProfileStyle.innerText}>Tel:</Text>
            <Text style={ProfileStyle.innerText}>{userInfo.Tel}</Text>
          </View>
          <View style={ProfileStyle.text}>
            <Text style={ProfileStyle.innerText}>Gender:</Text>
            <Text style={ProfileStyle.innerText}>{userInfo.Gender}</Text>
          </View>
        </View>
      </ScrollView >
    </SafeAreaView >
  )
}
const mapStateToProps = state => {
  return {
    token: state.userReducer.token,
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);