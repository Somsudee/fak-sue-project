import { StyleSheet } from "react-native";

export var ProfileStyle = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  backButton: {
    position: 'absolute',
    top: 24,
    bottom: 0,
    right: 0,
    left: 0,
  },
  header: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
    backgroundColor: '#ffffff',
    borderBottomRightRadius: 40,
    borderBottomLeftRadius: 40,
    marginBottom: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  topic: {
    fontSize: 24,
    color: '#183747',
  },
  image: {
    height: 200,
    width: 200,
    borderRadius: 100,
  },
  text: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-around',
    margin: 16,
  },
  innerText: {
    color: '#6C6C6C',
    fontSize: 16,
  }
})