import React, { useState } from 'react';
import { SafeAreaView, ScrollView, View, Text, Alert } from 'react-native';
import { RegisterStyle } from 'src/screen/OtherScreen/Register/RegisterStyle';
import { IconButton, TextInput, Button, RadioButton, HelperText, Modal } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
import { CreateUser } from '../../../api';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getUserToken } from '../../../reducer/UserAction';

const Register = (props) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [tel, setTel] = useState('');
  const [gender, setGender] = useState('');
  const [token, setToken] = useState('');
  const [visible, setVisible] = useState(false);

  const checkUsername = username && username.length > 0;
  const checkPassword = password && password.length > 0;
  const checkFirstname = firstName && firstName.length > 0;
  const checkLastname = lastName && lastName.length > 0;
  const checkTel = tel && tel.length > 0 && tel.includes('-');
  const checkGender = gender && gender.length > 0;

  const { getUserToken } = props;
  const navigation = useNavigation();

  const onRegister = () => {
    var user = {
      Username: username,
      Password: password,
      FirstName: firstName,
      LastName: lastName,
      Tel: tel,
      Gender: gender,
    }
    CreateUser(user)
      .then(res => {
        console.log(res.data.token)
        setVisible(true)
        // navigation.navigate('UserPhoto', { token: token })
      })
      .catch(err => Alert.alert("Username", "Username already used."))

  }

  return (
    <SafeAreaView style={RegisterStyle.container} contentContainerStyle={{ flexGrow: 1 }}>
      <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
        <View style={RegisterStyle.header} >
          <IconButton
            icon="chevron-left"
            color='#183747'
            size={32}
            onPress={() => navigation.navigate('Login')}
            style={RegisterStyle.backButton}
          />
          <Text style={RegisterStyle.topic}>REGISTER</Text>
        </View>
        <View style={{ flex: 3, paddingHorizontal: 16, }}>
          <View style={RegisterStyle.couple}>
            <TextInput
              label='Firstname'
              value={firstName}
              onChangeText={text => setFirstName(text)}
              style={RegisterStyle.input}
            />
            <TextInput
              label='Lastname'
              value={lastName}
              onChangeText={text => setLastName(text)}
              style={RegisterStyle.input}
            />
          </View>
          <TextInput
            label='Username'
            value={username}
            onChangeText={text => setUsername(text)}
            style={RegisterStyle.input}
          />
          <View style={RegisterStyle.couple}>
            <TextInput
              label='Password'
              value={password}
              secureTextEntry={true}
              onChangeText={text => setPassword(text)}
              style={RegisterStyle.input}
            />
            <TextInput
              label='Confirm Password'
              value={confirmPassword}
              secureTextEntry={true}
              onChangeText={text => setConfirmPassword(text)}
              style={[RegisterStyle.input, { fontSize: 14 }]}
            />
          </View>
          <TextInput
            label='Phone number'
            value={tel}
            onChangeText={text => setTel(text)}
            style={RegisterStyle.input}
          />
          <HelperText
            type="error"
            visible={tel && tel.length > 0 && !tel.includes('-') ? true : false}
          >
            Number required  "000 - 000 - 0000"
        </HelperText>
          <View style={RegisterStyle.couple}>
            <View style={RegisterStyle.radioButton}>
              <RadioButton
                value="female"
                status={gender === 'female' ? 'checked' : 'unchecked'}
                onPress={() => setGender("female")}
              />
              <Text>Female</Text>
            </View>
            <View style={RegisterStyle.radioButton}>
              <RadioButton
                value="male"
                status={gender === 'male' ? 'checked' : 'unchecked'}
                onPress={() => setGender("male")}
              />
              <Text>Male</Text>
            </View>
          </View>
        </View>
        <View style={RegisterStyle.containerButton}>
          <Button color='#ffffff' mode='text' disabled={!checkUsername || !checkPassword || !checkFirstname || !checkLastname || !checkTel || !checkGender}
            style={
              !checkUsername || !checkPassword || !checkFirstname || !checkLastname || !checkTel || !checkGender ?
                [RegisterStyle.button, RegisterStyle.disableButton]
                :
                [RegisterStyle.button, RegisterStyle.confirm]
            } onPress={() => onRegister()}>Confirm</Button>
        </View>
      </ScrollView>
      <Modal
        visible={visible}
        onDismiss={() => setVisible(false)}
        style={RegisterStyle.modalContainer}
      >
        <View style={RegisterStyle.modalBox}>
          <Text style={{ color: '#C4C4C4', marginTop: 16 }}>Successful</Text>
          <Text style={{ marginBottom: 16 }}>🤩</Text>
          <Button color='#fff' mode='text' uppercase={false} onPress={() => navigation.navigate('Login')} style={{ backgroundColor: '#F49015', }}>Done</Button>
        </View>
      </Modal>
    </SafeAreaView >
  );
}
const mapStateToProps = state => {
  return {
    token: state.userReducer.token,
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({getUserToken}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Register);