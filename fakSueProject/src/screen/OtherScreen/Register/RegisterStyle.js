import { StyleSheet } from "react-native";

export var RegisterStyle = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  backButton: {
    position: 'absolute',
    top: 24,
    bottom: 0,
    right: 0,
    left: 0,
  },
  header: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
    backgroundColor: '#ffffff',
    borderBottomRightRadius: 40,
    borderBottomLeftRadius: 40,
    marginBottom: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  topic: {
    fontSize: 24,
    color: '#183747',
  },
  input: {
    flex: 1,
    margin: 10,
    backgroundColor: '#ffffff00',
    height: 50,
  },
  couple: {
    flex: 1,
    flexDirection: 'row',
    color: '#000',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  radioButton: {
    flex: 1,
    flexDirection: 'row',
    color: '#000',
    justifyContent: 'center',
    alignItems: 'center',
    margin: 8,
  },
  text: {
    color: '#ffffff',
  },
  containerButton: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 15,
    marginHorizontal: 10,
    paddingHorizontal: 16,
  },
  button: {
    borderRadius: 8,
    paddingHorizontal: 20,
    marginTop: 32,
  },
  confirm: {
    backgroundColor: '#FFA26B',
    marginBottom: 10,
    padding: 5
  },
  disableButton: {
    backgroundColor: '#DEDEDE',
    marginBottom: 10,
    padding: 5
  },
  modalContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalBox: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: 136,
    borderRadius: 8,
    backgroundColor: "#fff",
    marginHorizontal: 40,
  },
})