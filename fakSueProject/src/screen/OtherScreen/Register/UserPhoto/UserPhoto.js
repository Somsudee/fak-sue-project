import React, { useState } from 'react';
import { View, ScrollView, Text, Image, TouchableHighlight, Alert } from 'react-native';
import { UserPhotoStyle } from './UserPhotoStyle';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { IconButton, Button, Modal, } from 'react-native-paper';
import { CreateUserImage } from '../../../../api';
import { useNavigation } from '@react-navigation/native';
import ImagePicker from 'react-native-image-picker';
import { SafeAreaView } from 'react-native-safe-area-context';
import { getUserToken } from '../../../../reducer/UserAction';

const UserPhoto = (props) => {
  const [image, setImage] = useState('');
  const [visible, setVisible] = useState(false);

  const checkImage = image && image.length > 0;
  const navigation = useNavigation();
  const { getUserToken } = props;

  const uploadImage = () => {
    ImagePicker.showImagePicker({}, (image) => {
      console.log('image = ', image);
      if (image.didCancel) {
        console.log('User cancelled image picker');
      } else if (image.error) {
        console.log('ImagePicker Error: ', image.error);
      } else {
        const uri = image.uri;
        const type = image.type;
        const name = image.fileName;
        const source = {
          uri,
          type,
          name,
        }
        cloudinaryUpload(source)
      }
    });
  }

  const cloudinaryUpload = (photo) => {
    const data = new FormData()
    data.append('file', photo)
    data.append('upload_preset', 'bnsng043')
    data.append("cloud_name", "dscr7nf6j")
    fetch("https://api.cloudinary.com/v1_1/dscr7nf6j/image/upload", {
      method: "post",
      body: data
    }).then(res => res.json()).
      then(data => {
        getUser(props.route.params.token)
          .then(res => {
            console.log(res.user.ID)
            // CreateUserImage(data.secure_url, props.route.params.token)
            //   .then(res => {
            //     console.log('putimage: ' + res.data);
            //     setImage(data.secure_url)
            //     getUserToken(props.route.params.token)
            //   })
            //   .catch(() => {
            //     Alert.alert("Fail", "upload your picture is failed :(");
            //   })
          })
          .catch(err => console.error(err))
      }).catch(() => {
        Alert.alert("Upload", "An Error Occured While Uploading")
      })
  }

  return (
    <SafeAreaView style={UserPhotoStyle.container} contentContainerStyle={{ flexGrow: 1 }}>
      <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
        <View style={UserPhotoStyle.header} >
          <IconButton
            icon="chevron-left"
            color='#183747'
            size={32}
            onPress={() => navigation.goBack()}
            style={UserPhotoStyle.backButton}
          />
          <Text style={UserPhotoStyle.topic}>ADD PHOTO</Text>
        </View>
        <View style={{ flex: 3, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ color: '#6C6C6C', marginBottom: 60 }}>Touch to add photo</Text>
          <TouchableHighlight onPress={() => uploadImage()}>
            {
              checkImage ?
                <Image style={UserPhotoStyle.imageCreate} source={{ uri: image }} />
                :
                <Image
                  style={UserPhotoStyle.image}
                  source={require('src/asset/picture.png')}
                />
            }
          </TouchableHighlight>
          <Button color='#ffffff' mode='text' disabled={!checkImage}
            style={
              !checkImage ?
                [UserPhotoStyle.button, UserPhotoStyle.disableButton]
                :
                [UserPhotoStyle.button, UserPhotoStyle.confirm]
            } onPress={() => setVisible(true)}>confirm</Button>
        </View>
      </ScrollView>
      <Modal
        visible={visible}
        onDismiss={() => setVisible(false)}
        style={UserPhotoStyle.modalContainer}
      >
        <View style={UserPhotoStyle.modalBox}>
          <Text style={{ color: '#C4C4C4', marginTop: 16 }}>Successful</Text>
          <Text style={{ marginBottom: 16 }}>🤩</Text>
          <Button color='#fff' mode='text' uppercase={false} onPress={() => navigation.navigate('Menu')} style={{ backgroundColor: '#F49015', }}>Done</Button>
        </View>
      </Modal>
    </SafeAreaView>
  )
}
const mapStateToProps = state => {
  return {
    token: state.userReducer.token,
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({ getUserToken }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(UserPhoto);