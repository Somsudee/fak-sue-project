import { StyleSheet } from "react-native";

export var UserPhotoStyle = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  backButton: {
    position: 'absolute',
    top: 24,
    bottom: 0,
    right: 0,
    left: 0,
  },
  header: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
    backgroundColor: '#ffffff',
    borderBottomRightRadius: 40,
    borderBottomLeftRadius: 40,
    marginBottom: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  topic: {
    fontSize: 24,
    color: '#183747',
  },
  image: {
    height: 200,
    width: 200,
    marginBottom: 60,
  },
  button: {
    borderRadius: 8,
    paddingHorizontal: 20,
    marginTop: 32,
  },
  confirm: {
    backgroundColor: '#FFA26B',
    marginBottom: 10,
    padding: 5
  },
  disableButton: {
    backgroundColor: '#DEDEDE',
    marginBottom: 10,
    padding: 5
  },
  imageCreate: {
    height: 200,
    width: 200,
    borderRadius: 8,
    marginBottom: 60,
  },
  modalContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalBox: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: 136,
    borderRadius: 8,
    backgroundColor: "#fff",
    marginHorizontal: 40,
  },
})