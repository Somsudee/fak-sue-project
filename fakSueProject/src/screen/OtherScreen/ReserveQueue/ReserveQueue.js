import React, { useState, useEffect } from 'react';
import { View, ScrollView, Text, Image } from 'react-native';
import { Picker } from '@react-native-community/picker';
import { SafeAreaView } from 'react-native-safe-area-context';
import { ReserveQueueStyle } from './ReserveQueueStyle';
import { IconButton, Divider, Button, Modal, TextInput, HelperText } from 'react-native-paper';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { host, GetOneRest, CreateQueue, getUser } from '../../../api';
import { useNavigation } from '@react-navigation/native';

const ReserveQueue = (props) => {
  const [amount, setAmount] = useState(0);
  const [time, setTime] = useState(0);
  const [restInfo, setRestInfo] = useState([]);
  const [restImage, setRestImage] = useState([]);
  const [restName, setRestName] = useState('');
  const [user, setUser] = useState([]);

  const [visible, setVisible] = useState(false);
  const checkAmount = amount > 0;
  const checkTime = time && time.length > 0;

  const navigation = useNavigation();
  const { token, } = props;

  useEffect(() => {
    fetchRest()
    fetchUser()
  }, [])

  const fetchRest = () => {
    GetOneRest(props.route.params.id)
      .then(res => {
        setRestInfo(res.data.rest)
        setRestImage(res.data.restImage)
        setRestName(res.data.rest.RestName)
      })
      .catch(err => console.error(err))
  }

  const fetchUser = () => {
    getUser(token)
      .then(res => setUser(res.data))
      .catch(err => console.error(err))
  }

  const onReserveQueue = () => {
    var queue = {
      UserId: user.user.ID,
      RestId: props.route.params.id,
      AmountPerson: amount,
      Time: parseFloat(time),
      GotQueue: false,
    }
    console.log(queue)
    CreateQueue(queue, token)
      .then(() => {
        setVisible(true)
        navigation.navigate('Queue')
      })
      .catch(err => console.error(err))
  }

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }} contentContainerStyle={{ flexGrow: 1 }}>
      <ScrollView style={{ flex: 2 }}>
        <View style={ReserveQueueStyle.header} >
          <IconButton
            icon="chevron-left"
            color='#183747'
            size={32}
            onPress={() => navigation.goBack()}
            style={ReserveQueueStyle.backButton}
          />
          <Text style={ReserveQueueStyle.topic}>RESERVE</Text>
        </View>
        <View style={ReserveQueueStyle.RestInfoContainer}>
          <View>
            <Image
              style={ReserveQueueStyle.image}
              source={{ uri: `${host}/RestPic/${restImage.ID}/${restImage.filename}` }}
            />
          </View>
          <View>
            <Text style={[ReserveQueueStyle.text, { fontSize: 20 }]}>{restName.toUpperCase()}</Text>
          </View>
        </View>
        <View style={ReserveQueueStyle.personContainer}>
          <View>
            <Text style={ReserveQueueStyle.text}>How many person?</Text>
          </View>
          <View style={ReserveQueueStyle.contentContainer}>
            <IconButton
              icon="minus"
              color="#fff"
              style={{ backgroundColor: '#66B5F8' }}
              size={20}
              onPress={() => {
                amount > 0 ?
                  setAmount(amount - 1)
                  :
                  setAmount(amount)
              }}
            />
            <View style={ReserveQueueStyle.scoreBoard}>
              <Text style={ReserveQueueStyle.textScore}>
                {amount}
              </Text>
            </View>
            <IconButton
              icon="plus"
              color="#fff"
              style={{ backgroundColor: '#FFA26B' }}
              size={20}
              onPress={() => {
                amount < 10 ?
                  setAmount(amount + 1)
                  :
                  setAmount(amount)
              }}
            />
          </View>
        </View>
        <Divider style={ReserveQueueStyle.divider} />
        <View style={ReserveQueueStyle.timeContainer}>
          {/* <Text style={ReserveQueueStyle.text}>Select your time</Text> */}
          {/* <Picker
            selectedValue={time}
            style={{ height: 50, width: 250, color: '#C1C1C1' }}
            onValueChange={(itemValue) => setTime(itemValue)}>
            <Picker.Item label="select time" value={0} />
            <Picker.Item label="9:00" value={9.00} />
            <Picker.Item label="10:00" value={10.00} />
          </Picker> */}
          <TextInput
            label='Type your time'
            value={time}
            keyboardType='numeric'
            onChangeText={text => setTime(text)}
            style={{ height: 50, width: 250, color: '#C1C1C1', backgroundColor: "#fff" }}
          />
          <HelperText
            type="error"
            visible={!checkTime}
          >
            required " 10" for 10.00 am
            </HelperText>
        </View>
        <Button color='#ffffff' mode='text' disabled={!checkAmount || !checkTime}
          style={
            !checkAmount || !checkTime ?
              [ReserveQueueStyle.button, ReserveQueueStyle.disableButton]
              :
              [ReserveQueueStyle.button, ReserveQueueStyle.confirm]
          }
          onPress={() => onReserveQueue()}>Reserve</Button>

      </ScrollView>
      <Modal
        visible={visible}
        onDismiss={() => setVisible(false)}
        style={ReserveQueueStyle.modalContainer}
      >
        <View style={ReserveQueueStyle.modalBox}>
          <Text style={{ color: '#C4C4C4', marginTop: 16 }}>Hope you enjoy</Text>
          <Text style={{ marginBottom: 16 }}>😊</Text>
          <Button color='#fff' mode='text' uppercase={false} onPress={() => navigation.navigate('Menu')} style={{ backgroundColor: '#F49015', }}>Done</Button>
        </View>
      </Modal>
    </SafeAreaView >
  )
}
const mapStateToProps = state => {
  return {
    token: state.userReducer.token,
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({}, dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(ReserveQueue);