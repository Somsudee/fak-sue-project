import { StyleSheet } from "react-native";

export var ReserveQueueStyle = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch', backgroundColor: '#f0f'
  },
  backButton: {
    position: 'absolute',
    top: 24,
    bottom: 0,
    right: 0,
    left: 0,
  },
  header: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
    backgroundColor: '#ffffff',
    borderBottomRightRadius: 40,
    borderBottomLeftRadius: 40,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  topic: {
    fontSize: 24,
    color: '#183747',
  },
  containerImage: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  image: {
    height: 124,
    width: 180,
    borderRadius: 4,
  },
  button: {
    borderRadius: 8,
    paddingHorizontal: 20,
    marginTop: 32,
    marginHorizontal: 16,
  },
  confirm: {
    backgroundColor: '#FFA26B',
    marginBottom: 10,
    padding: 5
  },
  disableButton: {
    backgroundColor: '#DEDEDE',
    marginBottom: 10,
    padding: 5
  },
  text: {
    color: '#6C6C6C',
    fontSize: 16,
  },
  contentContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  scoreBoard: {
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 8,
    borderColor: '#C4C4C4',
    height: 48,
    width: 48,
    marginHorizontal: 24,
    marginVertical: 16,
  },
  textScore: {
    color: '#6C6C6C',
    fontSize: 24,
  },
  RestInfoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    margin: 16,
  },
  personContainer: {
    justifyContent: 'center',
    alignItems: 'stretch',
    margin: 8,
    marginHorizontal: 16,
  },
  divider: {
    borderWidth: 0.5,
    borderColor: '#D9D9D9',
    marginHorizontal: 16,
  },
  timeContainer: {
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    margin: 16,
    marginBottom: 40,
  }
})